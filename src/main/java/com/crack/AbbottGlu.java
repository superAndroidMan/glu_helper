package com.crack;

import com.abbottdiabetescare.flashglucose.sensorabstractionservice.AlarmConfiguration;
import com.abbottdiabetescare.flashglucose.sensorabstractionservice.NonActionableConfiguration;
import com.abbottdiabetescare.flashglucose.sensorabstractionservice.dataprocessing.AlgorithmResults;
import com.abbottdiabetescare.flashglucose.sensorabstractionservice.dataprocessing.Out;
import com.github.unidbg.AndroidEmulator;
import com.github.unidbg.Module;
import com.github.unidbg.arm.backend.DynarmicFactory;
import com.github.unidbg.linux.android.AndroidEmulatorBuilder;
import com.github.unidbg.linux.android.AndroidResolver;
import com.github.unidbg.linux.android.dvm.*;
import com.github.unidbg.memory.Memory;
import com.github.unidbg.virtualmodule.android.AndroidModule;
import com.no.bjorninge.librestate.LibreState;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class AbbottGlu extends AbstractJni {


    private static String SO_PATH = "/Users/superman/IdeaProjects/unidbg-server/libDataProcessing.so";
    private final AndroidEmulator emulator;
    private final Module module;
    private final VM vm;

    static {
        //防止打成jar包的时候找不到文件
        String soPath = "example_binaries/libDataProcessing.so";
        String appPath = "example_binaries/GlucosePhoneHelper.apk";
        ClassPathResource classPathResource = new ClassPathResource(soPath);
        ClassPathResource appPathResource = new ClassPathResource(appPath);

        SO_PATH = soPath;

        try {
            InputStream inputStream = classPathResource.getInputStream();
            Files.copy(inputStream, Paths.get("./libDataProcessing.so"), StandardCopyOption.REPLACE_EXISTING);
            InputStream appinputStream = appPathResource.getInputStream();
            Files.copy(appinputStream, Paths.get("./GlucosePhoneHelper.apk"), StandardCopyOption.REPLACE_EXISTING);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        AbbottGlu abbottGlu = new AbbottGlu();
        abbottGlu.callGluAlgorithm();
        System.out.println("----------------------------");
    }

    public AbbottGlu() {
//        emulator = new AndroidARMEmulator("com.xxx.offical"); // 创建模拟器实例，要模拟32位或者64位，在这里区分
        emulator = AndroidEmulatorBuilder.for32Bit()
                .addBackendFactory(new DynarmicFactory(true))
                .setProcessName("com.jiuming.glucosehelper").build();
        Memory memory = emulator.getMemory(); // 模拟器的内存操作接口
        memory.setLibraryResolver(new AndroidResolver(23));// 设置系统类库解析
        vm = emulator.createDalvikVM(new File("./GlucosePhoneHelper.apk")); // 创建Android虚拟机
        new AndroidModule(emulator, vm).register(memory);

        vm.setJni(this);
        vm.setVerbose(true);// 设置是否打印Jni调用细节

        // 自行修改文件路径,loadLibrary是java加载so的方法
        DalvikModule dm = vm.loadLibrary(new File("./libDataProcessing.so"), false); // 加载libcms.so到unicorn虚拟内存，加载成功以后会默认调用init_array等函数
        dm.callJNI_OnLoad(emulator);// 手动执行JNI_OnLoad函数
        module = dm.getModule();// 加载好的libcms.so对应为一个模块

    }

    public void callGluAlgorithm() {
        DvmClass dataProcessingNative = vm.resolveClass("com/abbottdiabetescare/flashglucose/sensorabstractionservice/dataprocessing/DataProcessingNative");
        AlarmConfiguration alarm_configuration = new AlarmConfiguration(70, 180);
        NonActionableConfiguration non_actionable_configuration = new NonActionableConfiguration(true, true, 0, 40, 500, -2, 2);
        int sensorStartTimestamp = 0x0e181349;
        int sensorScanTimestamp = 0x0e1c4794;
        int currentUtcOffset = 0x0036ee80;

        Out<Integer> out1 = new Out();
        Out<Integer> out2 = new Out();
        Out<Boolean> out3 = new Out();
        Out<Boolean> out4 = new Out();
        Out<byte[]> out = new Out();
        Out<AlgorithmResults> out5 = new Out();

        int parserType = 1095774808;

        try {

            int resultNumber = dataProcessingNative.callStaticJniMethodInt(emulator,"getProductFamily",parserType);
            System.out.println("test==========" + resultNumber);

            int resultNumber2 = dataProcessingNative.callStaticJniMethodInt(emulator,"getTotalMemorySize",parserType);
            System.out.println("test==========" + resultNumber2);

            DvmObject<?> result = dataProcessingNative.callStaticJniMethodObject(emulator, "processScan",
                    parserType,
                    alarm_configuration,
                    non_actionable_configuration,
                    getTestPackData(),
                    sensorStartTimestamp, sensorScanTimestamp, currentUtcOffset, LibreState.getDefaultState(), out1, out2, out3, out4, out, out5
            );
            System.out.println("test==========" + result.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    byte[] getTestPackData() {
        byte[] packet = {(byte) 0x3a, (byte) 0xcf, (byte) 0x10, (byte) 0x16, (byte) 0x03, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x4f, (byte) 0x11, (byte) 0x08, (byte) 0x10, (byte) 0xad, (byte) 0x02, (byte) 0xc8, (byte) 0xd4,
                (byte) 0x5b, (byte) 0x00, (byte) 0xaa, (byte) 0x02, (byte) 0xc8, (byte) 0xb4, (byte) 0x1b, (byte) 0x80,
                (byte) 0xa9, (byte) 0x02, (byte) 0xc8, (byte) 0x9c, (byte) 0x5b, (byte) 0x00, (byte) 0xa9, (byte) 0x02,
                (byte) 0xc8, (byte) 0x8c, (byte) 0x1b, (byte) 0x80, (byte) 0xb0, (byte) 0x02, (byte) 0xc8, (byte) 0x30,
                (byte) 0x5c, (byte) 0x80, (byte) 0xb0, (byte) 0x02, (byte) 0x88, (byte) 0xe6, (byte) 0x9c, (byte) 0x80,
                (byte) 0xb8, (byte) 0x02, (byte) 0xc8, (byte) 0x3c, (byte) 0x9d, (byte) 0x80, (byte) 0xb8, (byte) 0x02,
                (byte) 0xc8, (byte) 0x60, (byte) 0x9d, (byte) 0x80, (byte) 0xa1, (byte) 0x02, (byte) 0xc8, (byte) 0xdc,
                (byte) 0x9e, (byte) 0x80, (byte) 0xab, (byte) 0x02, (byte) 0xc8, (byte) 0x14, (byte) 0x9e, (byte) 0x80,
                (byte) 0xa9, (byte) 0x02, (byte) 0xc8, (byte) 0xc0, (byte) 0x9d, (byte) 0x80, (byte) 0xab, (byte) 0x02,
                (byte) 0xc8, (byte) 0x78, (byte) 0x9d, (byte) 0x80, (byte) 0xaa, (byte) 0x02, (byte) 0xc8, (byte) 0x40,
                (byte) 0x9d, (byte) 0x80, (byte) 0xa8, (byte) 0x02, (byte) 0xc8, (byte) 0x08, (byte) 0x9d, (byte) 0x80,
                (byte) 0xa8, (byte) 0x02, (byte) 0xc8, (byte) 0x2c, (byte) 0x5c, (byte) 0x80, (byte) 0xad, (byte) 0x02,
                (byte) 0xc8, (byte) 0xf8, (byte) 0x5b, (byte) 0x00, (byte) 0x29, (byte) 0x06, (byte) 0xc8, (byte) 0xf4,
                (byte) 0x9b, (byte) 0x80, (byte) 0xc9, (byte) 0x05, (byte) 0xc8, (byte) 0x8c, (byte) 0xde, (byte) 0x80,
                (byte) 0xc3, (byte) 0x05, (byte) 0xc8, (byte) 0x28, (byte) 0x9e, (byte) 0x80, (byte) 0x2c, (byte) 0x06,
                (byte) 0xc8, (byte) 0xd0, (byte) 0x9e, (byte) 0x80, (byte) 0x7b, (byte) 0x06, (byte) 0x88, (byte) 0xa6,
                (byte) 0x9e, (byte) 0x80, (byte) 0xf9, (byte) 0x05, (byte) 0xc8, (byte) 0xb0, (byte) 0x9e, (byte) 0x80,
                (byte) 0x99, (byte) 0x05, (byte) 0xc8, (byte) 0xf0, (byte) 0x9e, (byte) 0x80, (byte) 0x2e, (byte) 0x05,
                (byte) 0xc8, (byte) 0x00, (byte) 0x9f, (byte) 0x80, (byte) 0x81, (byte) 0x04, (byte) 0xc8, (byte) 0x48,
                (byte) 0xa0, (byte) 0x80, (byte) 0x5d, (byte) 0x04, (byte) 0xc8, (byte) 0x38, (byte) 0x9d, (byte) 0x80,
                (byte) 0x12, (byte) 0x04, (byte) 0xc8, (byte) 0x10, (byte) 0x9e, (byte) 0x80, (byte) 0xcf, (byte) 0x03,
                (byte) 0xc8, (byte) 0x4c, (byte) 0x9e, (byte) 0x80, (byte) 0x6f, (byte) 0x03, (byte) 0xc8, (byte) 0xb8,
                (byte) 0x9e, (byte) 0x80, (byte) 0x19, (byte) 0x03, (byte) 0xc8, (byte) 0x40, (byte) 0x9f, (byte) 0x80,
                (byte) 0xc5, (byte) 0x02, (byte) 0xc8, (byte) 0xf4, (byte) 0x9e, (byte) 0x80, (byte) 0xaa, (byte) 0x02,
                (byte) 0xc8, (byte) 0xf8, (byte) 0x5b, (byte) 0x00, (byte) 0xa2, (byte) 0x04, (byte) 0xc8, (byte) 0x38,
                (byte) 0x9a, (byte) 0x00, (byte) 0xd1, (byte) 0x04, (byte) 0xc8, (byte) 0x28, (byte) 0x9b, (byte) 0x80,
                (byte) 0xe4, (byte) 0x04, (byte) 0xc8, (byte) 0xe0, (byte) 0x1a, (byte) 0x80, (byte) 0x8f, (byte) 0x04,
                (byte) 0xc8, (byte) 0x20, (byte) 0x9b, (byte) 0x80, (byte) 0x22, (byte) 0x06, (byte) 0xc8, (byte) 0x50,
                (byte) 0x5b, (byte) 0x80, (byte) 0xbc, (byte) 0x06, (byte) 0xc8, (byte) 0x54, (byte) 0x9c, (byte) 0x80,
                (byte) 0x7f, (byte) 0x05, (byte) 0xc8, (byte) 0x24, (byte) 0x5c, (byte) 0x80, (byte) 0xc9, (byte) 0x05,
                (byte) 0xc8, (byte) 0x38, (byte) 0x5c, (byte) 0x80, (byte) 0x38, (byte) 0x05, (byte) 0xc8, (byte) 0xf4,
                (byte) 0x1a, (byte) 0x80, (byte) 0x37, (byte) 0x07, (byte) 0xc8, (byte) 0x84, (byte) 0x5b, (byte) 0x80,
                (byte) 0xfb, (byte) 0x08, (byte) 0xc8, (byte) 0x4c, (byte) 0x9c, (byte) 0x80, (byte) 0xfb, (byte) 0x09,
                (byte) 0xc8, (byte) 0x7c, (byte) 0x9b, (byte) 0x80, (byte) 0x77, (byte) 0x0a, (byte) 0xc8, (byte) 0xe4,
                (byte) 0x5a, (byte) 0x80, (byte) 0xdf, (byte) 0x09, (byte) 0xc8, (byte) 0x88, (byte) 0x9f, (byte) 0x80,
                (byte) 0x6d, (byte) 0x08, (byte) 0xc8, (byte) 0x2c, (byte) 0x9f, (byte) 0x80, (byte) 0xc3, (byte) 0x06,
                (byte) 0xc8, (byte) 0xb0, (byte) 0x9d, (byte) 0x80, (byte) 0xd9, (byte) 0x11, (byte) 0x00, (byte) 0x00,
                (byte) 0x72, (byte) 0xc2, (byte) 0x00, (byte) 0x08, (byte) 0x82, (byte) 0x05, (byte) 0x09, (byte) 0x51,
                (byte) 0x14, (byte) 0x07, (byte) 0x96, (byte) 0x80, (byte) 0x5a, (byte) 0x00, (byte) 0xed, (byte) 0xa6,
                (byte) 0x0e, (byte) 0x6e, (byte) 0x1a, (byte) 0xc8, (byte) 0x04, (byte) 0xdd, (byte) 0x58, (byte) 0x6d};
        return packet;
    }

}
