package com.abbottdiabetescare.flashglucose.sensorabstractionservice.dataprocessing;

/**
 * Created by LibreOOPAlgorithm.
 * User: Antier
 * Date: 2020/6/19
 * Time: 15:49
 * Describe:
 */
public enum DataProcessingResult {
    SUCCESS(0),
    RESTART_SENSOR_STORAGE_STATE(1),
    RESCAN_SENSOR_BAD_CRC(2),
    TERMINATE_SENSOR_NORMAL_TERMINATED_STATE(3),
    TERMINATE_SENSOR_ERROR_TERMINATED_STATE(4),
    TERMINATE_SENSOR_CORRUPT_PAYLOAD(5),
    FATAL_ERROR_BAD_ARGUMENTS(6);

    private int value;

    DataProcessingResult(int paramInt) {
        this.value = paramInt;
    }

    private static DataProcessingResult fromValue(int paramInt) {
        for (DataProcessingResult dataProcessingResult : values()) {
            if (dataProcessingResult.value == paramInt)
                return dataProcessingResult;
        }
        throw new IllegalArgumentException();
    }

    private int toValue() {
        return this.value;
    }
}
