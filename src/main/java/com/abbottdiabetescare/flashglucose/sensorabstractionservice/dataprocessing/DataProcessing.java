package com.abbottdiabetescare.flashglucose.sensorabstractionservice.dataprocessing;

import com.abbottdiabetescare.flashglucose.sensorabstractionservice.ApplicationRegion;
import com.abbottdiabetescare.flashglucose.sensorabstractionservice.AlarmConfiguration;
import com.abbottdiabetescare.flashglucose.sensorabstractionservice.NonActionableConfiguration;

/**
 * Created by LibreOOPAlgorithm.
 * User: Antier
 * Date: 2020/6/19
 * Time: 15:45
 * Describe:
 */
public interface DataProcessing {
    public static final int BASE_YEAR = 2010;

    boolean getNeedsReaderInfoForActivation();

    MemoryRegion getNextRegionToRead(byte[] paramArrayOfbyte, int paramInt);

    int getProductFamily();

    int getTotalMemorySize();

    int getUnlockCode();

    void initialize(Object paramObject);

    boolean isPatchSupported(byte[] paramArrayOfbyte, ApplicationRegion paramApplicationRegion);

    DataProcessingOutputs processScan(AlarmConfiguration paramAlarmConfiguration, NonActionableConfiguration paramNonActionableConfiguration, byte[] paramArrayOfbyte1, int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfbyte2) throws DataProcessingException;
}
