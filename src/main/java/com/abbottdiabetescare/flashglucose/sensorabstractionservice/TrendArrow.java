package com.abbottdiabetescare.flashglucose.sensorabstractionservice;

/**
 * Created by LibreOOPAlgorithm.
 * User: Antier
 * Date: 2020/6/19
 * Time: 15:39
 * Describe:
 */
public enum TrendArrow {
    NOT_DETERMINED(0),
    FALLING_QUICKLY(1),
    FALLING(2),
    STABLE(3),
    RISING(4),
    RISING_QUICKLY(5);

    private int value;

    TrendArrow(int paramInt) {
        this.value = paramInt;
    }

    private static TrendArrow fromValue(int paramInt) {
        for (TrendArrow trendArrow : values()) {
            if (trendArrow.value == paramInt)
                return trendArrow;
        }
        throw new IllegalArgumentException();
    }

    private int toValue() {
        return this.value;
    }
}
