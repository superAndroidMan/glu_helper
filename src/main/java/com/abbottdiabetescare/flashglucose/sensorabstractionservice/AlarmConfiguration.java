package com.abbottdiabetescare.flashglucose.sensorabstractionservice;

/**
 * Created by LibreOOPAlgorithm.
 * User: Antier
 * Date: 2020/6/19
 * Time: 15:24
 * Describe:
 */
public class AlarmConfiguration {
    private int highGlucoseAlarmThreshold;

    private int lowGlucoseAlarmThreshold;

    public AlarmConfiguration(int paramInt1, int paramInt2) {
        this.lowGlucoseAlarmThreshold = paramInt1;
        this.highGlucoseAlarmThreshold = paramInt2;
    }

    public int getHighGlucoseAlarmThreshold() {
        return this.highGlucoseAlarmThreshold;
    }

    public int getLowGlucoseAlarmThreshold() {
        return this.lowGlucoseAlarmThreshold;
    }
}
