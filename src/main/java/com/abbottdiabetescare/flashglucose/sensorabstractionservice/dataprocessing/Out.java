package com.abbottdiabetescare.flashglucose.sensorabstractionservice.dataprocessing;

/**
 * Created by LibreOOPAlgorithm.
 * User: Antier
 * Date: 2020/6/19
 * Time: 15:50
 * Describe:
 */
public class Out<T> {
    private T value;

    public T value() {
        return this.value;
    }

    public void value(T paramT) {
        this.value = paramT;
    }

}
