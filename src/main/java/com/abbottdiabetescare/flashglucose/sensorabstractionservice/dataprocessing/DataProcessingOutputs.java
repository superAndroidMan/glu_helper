package com.abbottdiabetescare.flashglucose.sensorabstractionservice.dataprocessing;

/**
 * Created by LibreOOPAlgorithm.
 * User: Antier
 * Date: 2020/6/19
 * Time: 15:26
 * Describe:
 */
public class DataProcessingOutputs {
    private final AlgorithmResults algorithmResults;

    private final int estimatedSensorEndTimestamp;

    private final int estimatedSensorStartTimestamp;

    private final boolean insertionIsConfirmed;

    private final byte[] newState;

    private final boolean sensorHasBeenRemoved;

    public DataProcessingOutputs(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, byte[] paramArrayOfbyte, AlgorithmResults paramAlgorithmResults) {
        this.estimatedSensorStartTimestamp = paramInt1;
        this.estimatedSensorEndTimestamp = paramInt2;
        this.insertionIsConfirmed = paramBoolean1;
        this.sensorHasBeenRemoved = paramBoolean2;
        this.newState = paramArrayOfbyte;
        this.algorithmResults = paramAlgorithmResults;
    }

    public AlgorithmResults getAlgorithmResults() {
        return this.algorithmResults;
    }

    public int getEstimatedSensorEndTimestamp() {
        return this.estimatedSensorEndTimestamp;
    }

    public int getEstimatedSensorStartTimestamp() {
        return this.estimatedSensorStartTimestamp;
    }

    public boolean getInsertionIsConfirmed() {
        return this.insertionIsConfirmed;
    }

    public byte[] getNewState() {
        return this.newState;
    }

    public boolean getSensorHasBeenRemoved() {
        return this.sensorHasBeenRemoved;
    }
}
