package com.abbottdiabetescare.flashglucose.sensorabstractionservice.dataprocessing;

/**
 * Created by LibreOOPAlgorithm.
 * User: Antier
 * Date: 2020/6/19
 * Time: 15:26
 * Describe:
 */
public class DataProcessingException extends Exception {
    private static long serialVersionUID = 1L;

    private DataProcessingResult result;

    public DataProcessingException(DataProcessingResult paramDataProcessingResult) {
        super(paramDataProcessingResult.toString());
        this.result = paramDataProcessingResult;
    }

    public DataProcessingResult getResult() {
        return this.result;
    }
}
