package com.abbottdiabetescare.flashglucose.sensorabstractionservice;

/**
 * Created by LibreOOPAlgorithm.
 * User: Antier
 * Date: 2020/6/19
 * Time: 15:25
 * Describe:
 */
public enum ApplicationRegion {
    LEVEL_1(0),
    LEVEL_2(1),
    LEVEL_3(2),
    LEVEL_4(3),
    LEVEL_5(4),
    LEVEL_6(5);

    private int value;

    ApplicationRegion(int paramInt) {
        this.value = paramInt;
    }

    private ApplicationRegion fromValue(int paramInt) {
        for (ApplicationRegion applicationRegion : values()) {
            if (applicationRegion.value == paramInt)
                return applicationRegion;
        }
        throw new IllegalArgumentException();
    }

    private int toValue() {
        return this.value;
    }

}
