package com.abbottdiabetescare.flashglucose.sensorabstractionservice.dataprocessing;

/**
 * Created by LibreOOPAlgorithm.
 * User: Antier
 * Date: 2020/6/19
 * Time: 15:47
 * Describe:
 */
public class MemoryRegion {
    private final int numberOfBytes;

    private final int startAddress;

    public MemoryRegion(int paramInt1, int paramInt2) {
        this.startAddress = paramInt1;
        this.numberOfBytes = paramInt2;
    }

    public int getNumberOfBytes() {
        return this.numberOfBytes;
    }

    public int getStartAddress() {
        return this.startAddress;
    }
}
