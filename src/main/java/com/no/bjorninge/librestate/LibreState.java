package com.no.bjorninge.librestate;


import java.util.Arrays;

public class LibreState {
    private static final String TAG = "xOOPAlgorithm state";
    private static String SAVED_STATE =  "savedstate";
    private static String SAVED_SENDOR_ID = "savedstatesensorid";

    private static byte[] defaultState = {(byte) 0xff, (byte) 0xff, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
            (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
            (byte) 0xff, (byte) 0xff, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
            (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00};

    public static byte[] getDefaultState(){
        return Arrays.copyOf(defaultState, defaultState.length);
    }

}
