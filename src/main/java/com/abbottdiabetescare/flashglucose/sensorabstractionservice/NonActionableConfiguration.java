package com.abbottdiabetescare.flashglucose.sensorabstractionservice;

/**
 * Created by LibreOOPAlgorithm.
 * User: Antier
 * Date: 2020/6/19
 * Time: 15:25
 * Describe:
 */
public class NonActionableConfiguration {
    private boolean isEnabled;

    private boolean isVelocityCheckEnabled;

    private int maximumActionableValue;

    private double maximumPositiveActionableVelocity;

    private int minimumActionableId;

    private int minimumActionableValue;

    private double minimumNegativeActionableVelocity;

    public NonActionableConfiguration(boolean paramBoolean1, boolean paramBoolean2, int paramInt1, int paramInt2, int paramInt3, double paramDouble1, double paramDouble2) {
        this.isEnabled = paramBoolean1;
        this.isVelocityCheckEnabled = paramBoolean2;
        this.minimumActionableId = paramInt1;
        this.minimumActionableValue = paramInt2;
        this.maximumActionableValue = paramInt3;
        this.minimumNegativeActionableVelocity = paramDouble1;
        this.maximumPositiveActionableVelocity = paramDouble2;
    }

    public boolean getIsEnabled() {
        return this.isEnabled;
    }

    public boolean getIsVelocityCheckEnabled() {
        return this.isVelocityCheckEnabled;
    }

    public int getMaximumActionableValue() {
        return this.maximumActionableValue;
    }

    public double getMaximumPositiveActionableVelocity() {
        return this.maximumPositiveActionableVelocity;
    }

    public int getMinimumActionableId() {
        return this.minimumActionableId;
    }

    public int getMinimumActionableValue() {
        return this.minimumActionableValue;
    }

    public double getMinimumNegativeActionableVelocity() {
        return this.minimumNegativeActionableVelocity;
    }
}
