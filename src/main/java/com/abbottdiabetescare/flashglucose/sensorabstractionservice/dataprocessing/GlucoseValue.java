package com.abbottdiabetescare.flashglucose.sensorabstractionservice.dataprocessing;

/**
 * Created by LibreOOPAlgorithm.
 * User: Antier
 * Date: 2020/6/19
 * Time: 15:26
 * Describe:
 */
public class GlucoseValue {
    private final int dataQuality;

    private final int id;

    private final int value;

    public GlucoseValue(int paramInt1, int paramInt2, int paramInt3) {
        this.id = paramInt1;
        this.dataQuality = paramInt2;
        this.value = paramInt3;
    }

    public int getDataQuality() {
        return this.dataQuality;
    }

    public int getId() {
        return this.id;
    }

    public int getValue() {
        return this.value;
    }
}
