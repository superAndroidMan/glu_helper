package com.abbottdiabetescare.flashglucose.sensorabstractionservice.dataprocessing;

import com.abbottdiabetescare.flashglucose.sensorabstractionservice.Alarm;

import java.util.List;

import com.abbottdiabetescare.flashglucose.sensorabstractionservice.TrendArrow;

/**
 * Created by LibreOOPAlgorithm.
 * User: Antier
 * Date: 2020/6/19
 * Time: 15:31
 * Describe:
 */
public class AlgorithmResults {
    private Alarm alarm;

    private List<GlucoseValue> historicGlucose;

    private boolean isActionable;

    private GlucoseValue realTimeGlucose;

    private TrendArrow trendArrow;

    public AlgorithmResults(List<GlucoseValue> paramList, GlucoseValue paramGlucoseValue, TrendArrow paramTrendArrow, Alarm paramAlarm, boolean paramBoolean) {
        this.historicGlucose = paramList;
        this.realTimeGlucose = paramGlucoseValue;
        this.trendArrow = paramTrendArrow;
        this.alarm = paramAlarm;
        this.isActionable = paramBoolean;
    }

    public Alarm getAlarm() {
        return this.alarm;
    }

    public List<GlucoseValue> getHistoricGlucose() {
        return this.historicGlucose;
    }

    public boolean getIsActionable() {
        return this.isActionable;
    }

    public GlucoseValue getRealTimeGlucose() {
        return this.realTimeGlucose;
    }

    public TrendArrow getTrendArrow() {
        return this.trendArrow;
    }
}
