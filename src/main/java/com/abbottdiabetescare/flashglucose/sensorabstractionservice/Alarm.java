package com.abbottdiabetescare.flashglucose.sensorabstractionservice;

/**
 * Created by LibreOOPAlgorithm.
 * User: Antier
 * Date: 2020/6/19
 * Time: 15:32
 * Describe:
 */
public enum Alarm {
    NOT_DETERMINED(0),
    LOW_GLUCOSE(1),
    PROJECTED_LOW_GLUCOSE(2),
    GLUCOSE_OK(3),
    PROJECTED_HIGH_GLUCOSE(4),
    HIGH_GLUCOSE(5);

    private int value;

    Alarm(int paramInt) {
        this.value = paramInt;
    }

    private static Alarm fromValue(int paramInt) {
        for (Alarm alarm : values()) {
            if (alarm.value == paramInt)
                return alarm;
        }
        throw new IllegalArgumentException();
    }

    private int toValue() {
        return this.value;
    }
}
